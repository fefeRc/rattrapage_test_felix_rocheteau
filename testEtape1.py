import unittest
import main


class TestMathFunctions(unittest.TestCase):

    def test_add_numbers(self):
        self.assertEqual(main.add_numbers(2, 2), 4)
        self.assertEqual(main.add_numbers(0, 0), 0)
        self.assertEqual(main.add_numbers(-1, 1), 0)
        self.assertEqual(main.add_numbers(100, -50), 50)

    def test_multiply_numbers(self):
        self.assertEqual(main.multiply_numbers(2, 2), 4)
        self.assertEqual(main.multiply_numbers(0, 5), 0)
        self.assertEqual(main.multiply_numbers(-2, 5), -10)
        self.assertEqual(main.multiply_numbers(10, -3), -30)

    def test_divide_numbers(self):
        self.assertEqual(main.divide_numbers(10, 2), 5)
        self.assertEqual(main.divide_numbers(5, 2), 2)
        self.assertEqual(main.divide_numbers(-10, 3), -4)
        self.assertEqual(main.divide_numbers(20, -3), -7)

    def test_subtract_numbers(self):
        self.assertEqual(main.subtract_numbers(10, 2), 8)
        self.assertEqual(main.subtract_numbers(5, 2), 3)
        self.assertEqual(main.subtract_numbers(-10, 3), -13)
        self.assertEqual(main.subtract_numbers(20, -3), 23)


if __name__ == '__main__':
    unittest.main()
