import unittest
import main


class TestCalculatriceEnfantFrancais(unittest.TestCase):
    def test_calculatrice_enfant_francais_somme(self):
        self.assertEqual(main.calculatrice_enfant_francais("2 plus 3"), 5)
        self.assertEqual(main.calculatrice_enfant_francais("10 plus 7"), 17)
        self.assertEqual(main.calculatrice_enfant_francais("-5 plus 8"), 3)

    def test_calculatrice_enfant_francais_expression_invalide(self):
        with self.assertRaises(ValueError):
            main.calculatrice_enfant_francais("2 plus")
        with self.assertRaises(ValueError):
            main.calculatrice_enfant_francais("deux plus 3")
        with self.assertRaises(ValueError):
            main.calculatrice_enfant_francais("2 plus 3 plus 4")
        with self.assertRaises(ValueError):
            main.calculatrice_enfant_francais("2 moins 3")


if __name__ == '__main__':
    unittest.main()
