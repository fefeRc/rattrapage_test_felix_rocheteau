import unittest
import main


class TestCalculatriceEnfantFrancais(unittest.TestCase):
    def test_calculatrice_enfant_francais_addition(self):
        self.assertEqual(main.calculatrice_enfant_francais_precision(
            '2.1234 plus 3.5678', 4), 5.6912)

    def test_calculatrice_enfant_francais_soustraction(self):
        self.assertEqual(main.calculatrice_enfant_francais_precision(
            '5.4321 moins 1.2345', 4), 4.1976)


if __name__ == '__main__':
    unittest.main()
