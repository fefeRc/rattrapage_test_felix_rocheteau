Environnement Virtuel:

    1 - Initialisez un environnement virtuel. 
        Cela permettra d'isoler les dépendances du projet et d'éviter les conflits avec d'autres projets Python.
        Utilisez la commande python3 -m venv venv pour créer un environnement virtuel nommé "venv".

    2 - Activez l'environnement virtuel.
        Sur Linux ou macOS, utilisez la commande source venv/bin/activate.
        Sur Windows, utilisez la commande venv\Scripts\activate.

    3 - N'oubliez pas de désactiver l'environnement virtuel lorsque vous avez terminé en utilisant la commande deactivate.


Imports
    Calc -> pip install calc