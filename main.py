
# ETAPE 1 - LES BASES______________________________________

# Situation 1
def add_numbers(x, y):
    return x + y

# Situation 2


def multiply_numbers(x, y):
    return x * y

# Situation 3


def divide_numbers(x, y):
    return x // y

# Situation 4


def subtract_numbers(x, y):
    return x - y


# ETAPE 2 - LES DECIMAUX_________________________________

# Situation 1
def addition(x, y, p):
    return round(x + y, p)

# Situation 2


def multiplication(x, y, p):
    return round(x * y, p)

# Situation 3


def division(x, y, p):
    return round(x / y, p)

# Situation 4


def soustraction(x, y, p):
    return round(x - y, p)


# ETAPE 3 - TOLERANCE_____________________________________

def calculatrice_enfant_francais(expression):
    # On découpe l'expression en mots
    mots = expression.split()

    # On vérifie que la longueur est bien de 3 (x, plus, y)
    if len(mots) != 3:
        raise ValueError("Expression invalide")

    # On vérifie que les deux premiers mots sont bien des nombres
    try:
        x = int(mots[0])
        y = int(mots[2])
    except ValueError:
        raise ValueError("Expression invalide")

    # On vérifie que le deuxième mot est bien "plus"
    if mots[1] != "plus":
        raise ValueError("Expression invalide")

    # On retourne la somme des deux nombres
    return x + y


# ETAPE 4 - FIGNOLAGE_____________________________________

def calculatrice_enfant_francais_precision(expression, precision):
    # On découpe l'expression en mots
    mots = expression.split()

    # On vérifie que la longueur est bien de 3 (x, plus, y)
    if len(mots) != 3:
        raise ValueError("Expression invalide")

    # On vérifie que les deux premiers mots sont bien des nombres
    try:
        x = int(mots[0])
        y = int(mots[2])
    except ValueError:
        raise ValueError("Expression invalide")

    # On retourne la valeur selon le mot employé
    if mots[1] == "plus":
        return addition(x, y, precision)
    if mots[1] == "moins":
        return soustraction(x, y, precision)
