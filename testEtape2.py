import unittest
from calc import *
import main


class TestDecimaux(unittest.TestCase):

    def test_addition(self):
        self.assertEqual(main.addition(2.5, 1.5, 1), 4.0)
        self.assertEqual(main.addition(5.666, 3.333, 2), 9.0)
        self.assertEqual(main.addition(0.5, 0.5, 3), 1.000)

    def test_multiplication(self):
        self.assertEqual(main.multiplication(2.5, 1.5, 1), 3.8)
        self.assertEqual(main.multiplication(5.666, 3.333, 2), 18.89)
        self.assertEqual(main.multiplication(0.5, 0.5, 3), 0.250)

    def test_division(self):
        self.assertEqual(main.division(2.5, 1.5, 1), 1.7)
        self.assertEqual(main.division(5.666, 3.333, 2), 1.70)
        self.assertEqual(main.division(0.5, 0.5, 3), 1.000)

    def test_soustraction(self):
        self.assertEqual(main.soustraction(2.5, 1.5, 1), 1.0)
        self.assertEqual(main.soustraction(5.666, 3.333, 2), 2.33)
        self.assertEqual(main.soustraction(0.5, 0.5, 3), 0.000)


if __name__ == '__main__':
    unittest.main()
